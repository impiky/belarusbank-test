workers = 830 
worker_salary = 700
salary_budget = 9570000
monthly_fired_workers = 5

def budget_calc(workers, worker_salary, salary_budget, monthly_fired_workers):
    month = 0
    payed_salary = 0
    while True:
        if salary_budget - payed_salary < workers * worker_salary:
            return int(month)
        else:
            payed_salary += workers * worker_salary
            month += 1
            workers = workers - monthly_fired_workers
            
    
def worker_period_count(month, workers, monthly_fired_workers):
    for i in range(0, month):
        workers = workers - monthly_fired_workers
    return int(workers)


def workers_with_selected_salary_in_calculated_range(month, salary_budget, worker_salary):
    salary_budget_per_month = salary_budget / month
    workers_to_leave = salary_budget_per_month / worker_salary
    return int(workers_to_leave) 


time_range = budget_calc(workers, worker_salary, salary_budget, monthly_fired_workers)
print(time_range)
print(worker_period_count(13, workers, monthly_fired_workers))
print(workers_with_selected_salary_in_calculated_range(time_range, salary_budget, 1650))