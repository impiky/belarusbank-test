# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(484, 314)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 481, 301))
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.label = QtWidgets.QLabel(self.tab)
        self.label.setGeometry(QtCore.QRect(10, 10, 131, 16))
        self.label.setObjectName("label")
        self.textEdit = QtWidgets.QTextEdit(self.tab)
        self.textEdit.setGeometry(QtCore.QRect(10, 30, 131, 31))
        self.textEdit.setAutoFillBackground(False)
        self.textEdit.setObjectName("textEdit")
        self.textEdit_2 = QtWidgets.QTextEdit(self.tab)
        self.textEdit_2.setGeometry(QtCore.QRect(170, 30, 131, 31))
        self.textEdit_2.setObjectName("textEdit_2")
        self.label_2 = QtWidgets.QLabel(self.tab)
        self.label_2.setGeometry(QtCore.QRect(170, 10, 161, 16))
        self.label_2.setObjectName("label_2")
        self.textEdit_3 = QtWidgets.QTextEdit(self.tab)
        self.textEdit_3.setGeometry(QtCore.QRect(330, 30, 131, 31))
        self.textEdit_3.setObjectName("textEdit_3")
        self.label_3 = QtWidgets.QLabel(self.tab)
        self.label_3.setGeometry(QtCore.QRect(330, 10, 131, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.tab)
        self.label_4.setGeometry(QtCore.QRect(20, 120, 281, 16))
        self.label_4.setObjectName("label_4")
        self.label_4.hide()
        self.checkBox = QtWidgets.QCheckBox(self.tab)
        self.checkBox.setEnabled(True)
        self.checkBox.setGeometry(QtCore.QRect(20, 80, 171, 20))
        self.checkBox.setObjectName("checkBox")
        self.checkBox.toggled.connect(lambda : self.show_field())
        self.textEdit_4 = QtWidgets.QTextEdit(self.tab)
        self.textEdit_4.setGeometry(QtCore.QRect(20, 140, 131, 31))
        self.textEdit_4.setObjectName("textEdit_4")
        self.textEdit_4.setText("0")
        self.textEdit_4.hide()
        self.pushButton = QtWidgets.QPushButton(self.tab)
        self.pushButton.setGeometry(QtCore.QRect(20, 222, 131, 31))
        self.pushButton.setInputMethodHints(QtCore.Qt.ImhNone)
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(lambda: self.budget_calc(int(self.textEdit.toPlainText()),
         int(self.textEdit_2.toPlainText()), int(self.textEdit_3.toPlainText()), int(self.textEdit_4.toPlainText())))
        self.label_14 = QtWidgets.QLabel(self.tab)
        self.label_14.setGeometry(QtCore.QRect(200, 200, 61, 16))
        self.label_14.setObjectName("label_14")
        self.label_14.hide()
        self.textEdit_14 = QtWidgets.QTextEdit(self.tab)
        self.textEdit_14.setGeometry(QtCore.QRect(200, 220, 131, 31))
        self.textEdit_14.setObjectName("textEdit_14")
        self.textEdit_14.setReadOnly(True)
        self.textEdit_14.hide()
        self.pushButton.clicked.connect(lambda : self.label_14.show())
        self.pushButton.clicked.connect(lambda : self.textEdit_14.show())
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.textEdit_5 = QtWidgets.QTextEdit(self.tab_2)
        self.textEdit_5.setGeometry(QtCore.QRect(20, 30, 131, 31))
        self.textEdit_5.setObjectName("textEdit_5")
        self.label_5 = QtWidgets.QLabel(self.tab_2)
        self.label_5.setGeometry(QtCore.QRect(20, 10, 331, 16))
        self.label_5.setObjectName("label_5")
        self.pushButton_2 = QtWidgets.QPushButton(self.tab_2)
        self.pushButton_2.setGeometry(QtCore.QRect(10, 232, 131, 31))
        self.pushButton_2.setInputMethodHints(QtCore.Qt.ImhNone)
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(lambda: self.worker_period_count(int(self.textEdit_14.toPlainText()),
         int(self.textEdit.toPlainText()), int(self.textEdit_4.toPlainText())))
        self.textEdit_15 = QtWidgets.QTextEdit(self.tab_2)
        self.textEdit_15.setGeometry(QtCore.QRect(190, 230, 131, 31))
        self.textEdit_15.setObjectName("textEdit_15")
        self.label_15 = QtWidgets.QLabel(self.tab_2)
        self.label_15.setGeometry(QtCore.QRect(190, 210, 61, 16))
        self.label_15.setObjectName("label_15")
        self.label_15.hide()
        self.textEdit_15.setReadOnly(True)
        self.textEdit_15.hide()
        self.pushButton_2.clicked.connect(lambda : self.label_15.show())
        self.pushButton_2.clicked.connect(lambda : self.textEdit_15.show())
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.textEdit_6 = QtWidgets.QTextEdit(self.tab_3)
        self.textEdit_6.setGeometry(QtCore.QRect(20, 30, 131, 31))
        self.textEdit_6.setObjectName("textEdit_6")
        self.label_6 = QtWidgets.QLabel(self.tab_3)
        self.label_6.setGeometry(QtCore.QRect(20, 10, 131, 16))
        self.label_6.setObjectName("label_6")
        self.textEdit_13 = QtWidgets.QTextEdit(self.tab_3)
        self.textEdit_13.setGeometry(QtCore.QRect(180, 30, 131, 31))
        self.textEdit_13.setObjectName("textEdit_13")
        self.label_13 = QtWidgets.QLabel(self.tab_3)
        self.label_13.setGeometry(QtCore.QRect(180, 10, 161, 16))
        self.label_13.setObjectName("label_13")
        self.label_16 = QtWidgets.QLabel(self.tab_3)
        self.label_16.setGeometry(QtCore.QRect(190, 210, 61, 16))
        self.label_16.setObjectName("label_16")
        self.label_16.hide()
        self.textEdit_16 = QtWidgets.QTextEdit(self.tab_3)
        self.textEdit_16.setGeometry(QtCore.QRect(190, 230, 131, 31))
        self.textEdit_16.setObjectName("textEdit_16")
        self.textEdit_16.setReadOnly(True)
        self.textEdit_16.hide()
        self.pushButton_5 = QtWidgets.QPushButton(self.tab_3)
        self.pushButton_5.setGeometry(QtCore.QRect(10, 232, 131, 31))
        self.pushButton_5.setInputMethodHints(QtCore.Qt.ImhNone)
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(lambda: self.workers_with_selected_salary_in_calculated_range(int(self.textEdit_14.toPlainText()),
         int(self.textEdit_6.toPlainText()), int(self.textEdit_13.toPlainText())))
        self.tabWidget.addTab(self.tab_3, "")
        self.pushButton_5.clicked.connect(lambda : self.label_16.show())
        self.pushButton_5.clicked.connect(lambda : self.textEdit_16.show())
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.action2 = QtWidgets.QAction(MainWindow)
        self.action2.setObjectName("action2")
        self.action3 = QtWidgets.QAction(MainWindow)
        self.action3.setObjectName("action3")
        self.action4 = QtWidgets.QAction(MainWindow)
        self.action4.setObjectName("action4")

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    """
        На предприятии работает 830 сотрудников. З/п каждого сотрудника 700р в месяц. Бюджет з/п составляет 9 570 000р. Каждый месяц увольняются 5 сотрудников.
            1. На сколько хватит запланированного бюджета з/п. (в месяцах)?

        Данный метод производит расчёт запланированного бюджета. Алгоритм следующий:
        В бесконечном цикле производится проверка, не превысили ли выплаты работникам общий бюджет. Если превысили - возвращаем кол-во месяцев.
        Иначе, производим расчет выплат работникам за месяц, прибавляя счётчик месяцев. 
        Если в условии указано кол-во увольняемых за месяц работников - уменьшаем общее число работников на число уволенных за месяц, если данное число не указано - 
        продолжаем выполнение цикла с новой итерации.
    """
    def budget_calc(self ,workers, worker_salary, salary_budget, monthly_fired_workers=0):
        month = 0
        payed_salary = 0
        while True:
            if salary_budget - payed_salary < workers * worker_salary:
                return self.textEdit_14.setText("{}".format(int(month)))
            else:
                payed_salary += workers * worker_salary
                month += 1
                if monthly_fired_workers > 0:
                    workers = workers - monthly_fired_workers
                else:
                    continue

    
    """
        2. Сколько сотрудников останется на предприятии по истечению 13 месяцев?

        Итеративным циклом for отнимаем число уволенных за месяц сотрудников от общего числа сотрудников.
        Итерации соответствуют кол-ву указанных в условии месяцев.
    """
    def worker_period_count(self, month, workers, monthly_fired_workers):
        for i in range(0, month):
            workers = workers - monthly_fired_workers
        return self.textEdit_15.setText("{}".format(int(workers)))
    

    """
        3.Сколько сотрудников нужно оставить на предприятии,
        что бы с бюджетом в 9 570 000р з/п составляла 1650р в месяц при периоде времени равному ответу п.1 (сотрудники не увольняются)?

        Первым выражением получаем зарплату всех работников за месяц.
        Следующим выражением вычисляем число работников, которым возможна выплата заданной в условии зарплаты.
    """
    def workers_with_selected_salary_in_calculated_range(self, month, salary_budget, worker_salary):
        salary_budget_per_month = salary_budget / month
        workers_to_leave = salary_budget_per_month / worker_salary
        return self.textEdit_16.setText("{}".format(int(workers_to_leave)))


    def show_field(self):
        if self.checkBox.isChecked():
            self.label_4.show()
            self.textEdit_4.show()
        else:
            self.label_4.hide()
            self.textEdit_4.hide()


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "App"))
        self.label.setText(_translate("MainWindow", "Количество сотрудников"))
        self.label_2.setText(_translate("MainWindow", "Заработная плата сотрудника"))
        self.label_3.setText(_translate("MainWindow", "Бюджет з/п"))
        self.label_4.setText(_translate("MainWindow", "Количество увольняемых в месяц сотрудников"))
        self.checkBox.setText(_translate("MainWindow", "Увольняются ли сотрудники"))
        self.pushButton.setText(_translate("MainWindow", "Рассчитать"))
        self.label_14.setText(_translate("MainWindow", "Результат:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "1"))
        self.label_5.setText(_translate("MainWindow", "Срок для расчета количества оставшихся сотрудников(в мес.)"))
        self.pushButton_2.setText(_translate("MainWindow", "Рассчитать"))
        self.label_15.setText(_translate("MainWindow", "Результат:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "2"))
        self.label_6.setText(_translate("MainWindow", "Бюджет з/п"))
        self.label_13.setText(_translate("MainWindow", "Заработная плата сотрудника"))
        self.label_16.setText(_translate("MainWindow", "Результат:"))
        self.pushButton_5.setText(_translate("MainWindow", "Рассчитать"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("MainWindow", "3"))
        self.action2.setText(_translate("MainWindow", "2"))
        self.action3.setText(_translate("MainWindow", "3"))
        self.action4.setText(_translate("MainWindow", "4"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

